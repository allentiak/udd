#!/usr/bin/ruby

# Used by the PTS

$:.unshift('../../rlibs')
require 'udd-db'

puts "Content-type: text/plain\n\n"

DB = Sequel.connect(UDD_GUEST)

COMQ = "select bugs_packages.package, count(*) as cnt from bugs, bugs_packages where bugs.id = bugs_packages.id and status != 'done'"
NOPEND = "and bugs.id not in (select id from bugs_tags where tag in ('pending','fixed'))"
ENDQ = "group by bugs_packages.package"

rc_m = Hash::new { 0 }
DB["#{COMQ} #{NOPEND} and severity >= 'serious' #{ENDQ}"].all.sym2str.
  each { |r| rc_m[r['package']] = r['cnt'] }

ino_m = Hash::new { 0 }
DB["#{COMQ} #{NOPEND} and severity in ('important','normal') #{ENDQ}"].all.sym2str.
  each { |r| ino_m[r['package']] = r['cnt'] }

mw_m = Hash::new { 0 }
DB["#{COMQ} #{NOPEND} and severity in ('minor', 'wishlist') #{ENDQ}"].all.sym2str.
  each { |r| mw_m[r['package']] = r['cnt'] }

fp_m = Hash::new { 0 }
DB["#{COMQ} #{NOPEND} and bugs.id in (select id from bugs_tags where tag in ('pending','fixed')) #{ENDQ}"].all.sym2str.
  each { |r| fp_m[r['package']] = r['cnt'] }

patch_m = Hash::new { 0 }
DB["#{COMQ} #{NOPEND} and bugs.id in (select id from bugs_tags where tag = 'patch') and bugs.id not in (select id from bugs_tags where tag = 'wontfix') #{ENDQ}"].all.sym2str.
  each { |r| patch_m[r['package']] = r['cnt'] }

pkgs = (rc_m.keys + ino_m.keys + mw_m.keys + fp_m.keys + patch_m.keys).uniq.sort
pkgs.each do |pkg|
  print "#{pkg} "
  print "#{rc_m[pkg]} "
  print "#{ino_m[pkg]} "
  print "#{mw_m[pkg]} "
  print "#{fp_m[pkg]} "
  puts "#{patch_m[pkg]}"
end
